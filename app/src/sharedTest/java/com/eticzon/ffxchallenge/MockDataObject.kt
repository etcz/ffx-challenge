package com.eticzon.ffxchallenge

import com.eticzon.ffxchallenge.data.model.*
import com.eticzon.ffxchallenge.data.repository.RequestError
import com.eticzon.ffxchallenge.data.repository.RequestState
import com.eticzon.ffxchallenge.ui.article.ArticleListViewModel
import org.joda.time.DateTime

object MockData {

    val smallImage = Image(
            url = "http://www.example.com/foo.png",
            width = 375,
            height = 250)

    val mediumImage = Image(
            url = "http://www.example.com/bar.png",
            width = 1174,
            height = 783)

    val largeImage = Image(
            url = "http://www.example.com/baz.png",
            width = 1536,
            height = 1010)

    val author = Author(
            name = "John Doe",
            title = "Resources reporter",
            email = "john_doe@example.com",
            relatedAssets = listOf(),
            relatedImages = listOf())

    val brand = Brand(
            title = "AFR",
            orderNum = 1)

    val category = Category(
            name = "Life and Leisure",
            orderNum = 1,
            sectionPath = "/brand/life-and-leisure")

    val company = Company(
            id = 997146262,
            companyCode = "RIO",
            companyName = "RIO TINTO LIMITED",
            abbreviatedName = "Rio Tinto Ltd",
            exchange = "ASX",
            companyNumber = "4458404")

    val html = Html(embedCode = "<div>\\t<div id=\\\"houses\\\"></div><script type=\\\"text/javascript\\\" src=\\\"https://data.afr.com/scripts/pym.min.js\\\"></script> <script> new pym.Parent('houses', 'https://data.afr.com/2018/03mar/house-price-forecasts-hype/index.html', {}); </script> </div>")

    val liveArticle = LiveArticle(id = 42)

    val source = Source(tagId = "AFR Contributor")

    val video = Video(id = 42)

    val articleOverride = ArticleOverride(
            overrideAbstract = "Fancy a \$600 steak? Join the queue",
            overrideHeadline = """
                    In the new world of haute cuisine, chefs are jostling to outdo
                    each other with rare offerings you can't buy anywhere else.,
                    """.trimIndent(),
            overrideIdentifier = "")

    val firstArticle = Article(
            id = 1029484477,
            categories = listOf(category),
            authors = listOf(author),
            url = "http://www.afr.com/lifestyle/food-and-wine/fine-dining/top-restaurants-go-bespoke-with-600-steaks-and-rare-fanshell-razor-clams-20180313-h0xfb1",
            brands = listOf(brand),
            lastModified = DateTime.parse("2018-03-19T06:15:00.000Z"),
            onTime = 1521465300000,
            sponsored = false,
            relatedImages = listOf(smallImage, mediumImage, largeImage),
            timeStamp = 1521465300000,
            identifier = "",
            byLine = "John Doe",
            acceptComments = false,
            sources = listOf(source),
            theAbstract = "In the new world of haute cuisine, chefs are jostling to outdo each other with rare offerings you can't buy anywhere else.",
            legalStatus = "None",
            override = articleOverride,
            numberOfComments = 0,
            companies = listOf(company),
            tabletHeadline = "",
            indexHeadline = "Fancy a $600 steak? Join the queue",
            headline = "Top restaurants go bespoke with $600 steaks and rare fan-shell razor clams")

    val secondArticle = Article(
            id = 1029484478,
            categories = listOf(category),
            authors = listOf(author),
            url = "http://www.afr.com/lifestyle/food-and-wine/fine-dining/top-restaurants-go-bespoke-with-600-steaks-and-rare-fanshell-razor-clams-20180313-h0xfb1",
            brands = listOf(brand),
            lastModified = DateTime.parse("2018-03-19T06:15:01.000Z"),
            onTime = 1521465301000,
            sponsored = false,
            relatedImages = listOf(smallImage, mediumImage, largeImage),
            timeStamp = 1521465301000,
            identifier = "",
            byLine = "John Doe",
            acceptComments = false,
            sources = listOf(source),
            theAbstract = "In the new world of haute cuisine, chefs are jostling to outdo each other with rare offerings you can't buy anywhere else.",
            legalStatus = "None",
            override = articleOverride,
            numberOfComments = 0,
            companies = listOf(company),
            tabletHeadline = "",
            indexHeadline = "Fancy a $600 steak? Join the queue",
            headline = "Top restaurants go bespoke with $600 steaks and rare fan-shell razor clams")

    val thirdArticle = Article(
            id = 1029484479,
            categories = listOf(category),
            authors = listOf(author),
            url = "http://www.afr.com/lifestyle/food-and-wine/fine-dining/top-restaurants-go-bespoke-with-600-steaks-and-rare-fanshell-razor-clams-20180313-h0xfb1",
            brands = listOf(brand),
            lastModified = DateTime.parse("2018-03-19T06:15:02.000Z"),
            onTime = 1521465302000,
            sponsored = false,
            relatedImages = listOf(smallImage, mediumImage, largeImage),
            timeStamp = 1521465302000,
            identifier = "",
            byLine = "John Doe",
            acceptComments = false,
            sources = listOf(source),
            theAbstract = "In the new world of haute cuisine, chefs are jostling to outdo each other with rare offerings you can't buy anywhere else.",
            legalStatus = "None",
            override = articleOverride,
            numberOfComments = 0,
            companies = listOf(company),
            tabletHeadline = "",
            indexHeadline = "Fancy a $600 steak? Join the queue",
            headline = "Top restaurants go bespoke with $600 steaks and rare fan-shell razor clams")

    val assetListWithThreeArticles = AssetList(
            displayName = "AFR Editor's Choice",
            assets = listOf(firstArticle, secondArticle, thirdArticle))

    val savedViewModelCompleted = ArticleListViewModel(
            title = "Breaking News",
            articles = listOf(firstArticle, secondArticle, thirdArticle),
            requestState = RequestState.COMPLETE,
            errorType = RequestError.NONE,
            errorMessage = "")

    val savedViewModelInFlight = ArticleListViewModel(
            title = "",
            articles = listOf(),
            requestState = RequestState.LOADING,
            errorType = RequestError.NONE,
            errorMessage = "")
}
