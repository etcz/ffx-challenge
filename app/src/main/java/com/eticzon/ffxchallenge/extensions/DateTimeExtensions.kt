package com.eticzon.ffxchallenge.extensions

import org.joda.time.*

fun DateTime.relativeElapsedTime(): String {
    val start = Instant(millis)
    val now = DateTime.now()

    val years = Years.yearsBetween(start, now).years
    val months = Months.monthsBetween(start, now).months
    val days = Days.daysBetween(start, now).days
    val hours = Hours.hoursBetween(start, now).hours
    val minutes = Minutes.minutesBetween(start, now).minutes

    return when {
        years > 1    -> "$years years ago"
        years == 1   -> "A year ago"
        months > 1   -> "$months months ago"
        months == 1  -> "A month ago"
        days > 1     -> "$days days ago"
        days == 1    -> "A day ago"
        hours > 1    -> "$hours hours ago"
        hours == 1   -> "An hour ago"
        minutes > 1  -> "$minutes minutes ago"
        minutes == 1 -> "A minute ago"
        else         -> "Just now"
    }
}