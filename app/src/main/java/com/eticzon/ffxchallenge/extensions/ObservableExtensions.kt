package com.eticzon.ffxchallenge.extensions

import io.reactivex.ObservableTransformer
import io.reactivex.Scheduler

fun <T> applySchedulers(s: Scheduler, o: Scheduler): ObservableTransformer<T, T> {
    return ObservableTransformer { observable ->
        observable.subscribeOn(s).observeOn(o)
    }
}
