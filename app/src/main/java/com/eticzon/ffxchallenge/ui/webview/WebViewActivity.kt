package com.eticzon.ffxchallenge.ui.webview

import android.os.Bundle
import android.view.View
import com.eticzon.ffxchallenge.R
import com.eticzon.ffxchallenge.data.repository.RequestError
import com.eticzon.ffxchallenge.di.AppComponent
import com.eticzon.ffxchallenge.di.WebViewModule
import com.eticzon.ffxchallenge.ui.base.BaseActivity
import com.eticzon.ffxchallenge.ui.widgets.TransitionFragment
import kotlinx.android.synthetic.main.activity_web_view.*
import javax.inject.Inject

class WebViewActivity : BaseActivity(), WebViewContract.View {

    companion object {
        const val PARAM_EXTERNAL_URL = "param.external_url"
        const val TAG_TRANSITION_FRAGMENT = "tag.transition_fragment"
    }

    @Inject lateinit var client: ObservableWebClient
    @Inject lateinit var presenter: WebViewPresenter

    private val transitionFragment
        get() = supportFragmentManager.findFragmentById(R.id.fragmentContainer) as TransitionFragment?

    override fun injectDependencies(appComponent: AppComponent) {
        appComponent.plus(WebViewModule(this))
                .injectTo(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_web_view)

        val fragment = if (savedInstanceState != null) {
            supportFragmentManager.findFragmentByTag(TAG_TRANSITION_FRAGMENT)
        } else {
            TransitionFragment.loadingInstance()
        }

        fragment?.let {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, fragment, TAG_TRANSITION_FRAGMENT)
                    .commit()

        }

        with (webview) {
            visibility = View.GONE

            webViewClient = client.webViewClient
            webChromeClient = client.webChromeClient

            settings.javaScriptEnabled = true /*（／_＼）*/
            settings.setAppCacheEnabled(true)
        }
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        presenter.attach()
    }

    override fun onStop() {
        super.onStop()
        presenter.detach()
    }

    override fun showLoading() {
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
        webview.visibility = View.VISIBLE
        if (transitionFragment != null) {
            supportFragmentManager.beginTransaction()
                    .remove(transitionFragment)
                    .commit()
        }
    }

    override fun loadContent() {
        webview.loadUrl(intent.getStringExtra(PARAM_EXTERNAL_URL))
    }

    override fun setLoadProgress(percent: Int) {
        transitionFragment?.loadProgress = percent
    }

    override fun showError(text: String, type: RequestError) {
        webview.visibility = View.GONE
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, TransitionFragment.errorInstance(text, type), TAG_TRANSITION_FRAGMENT)
                .commit()
    }
}
