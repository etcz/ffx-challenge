package com.eticzon.ffxchallenge.ui.webview

import android.webkit.*
import com.eticzon.ffxchallenge.data.repository.RequestState
import io.reactivex.subjects.BehaviorSubject
import java.lang.ref.WeakReference

class ProgressTrackerWebClient: ObservableWebClient {

    override val webViewClient: WebViewClient = ErrorListenerWebView(WeakReference(this))
    override val webChromeClient: WebChromeClient = ProgressListenerWebView(WeakReference(this))

    val state: BehaviorSubject<WebRequestState> by lazy {
        BehaviorSubject.createDefault(WebRequestState())
    }

    override fun getObservable(): BehaviorSubject<WebRequestState> = state

    class ProgressListenerWebView(private val client: WeakReference<ProgressTrackerWebClient>) : WebChromeClient() {
        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            when (newProgress) {
                0 -> client.get()?.state?.onNext(WebRequestState(state = RequestState.IDLE))
                in 1..80 -> client.get()?.state?.onNext(WebRequestState(state = RequestState.LOADING, progress = newProgress))
                else -> client.get()?.state?.onNext(WebRequestState(state = RequestState.COMPLETE))
            }
        }
    }

    class ErrorListenerWebView(private val client: WeakReference<ProgressTrackerWebClient>) : WebViewClient() {
        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            super.onReceivedError(view, request, error)
            client.get()?.state?.onNext(WebRequestState(state = RequestState.ERROR, t = Exception("Error loading resource")))
        }
    }
}