package com.eticzon.ffxchallenge.ui.navigation

enum class RouteName {
    ARTICLE,
    WEB_VIEW
}