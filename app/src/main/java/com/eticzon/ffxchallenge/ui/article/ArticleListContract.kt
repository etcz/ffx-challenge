package com.eticzon.ffxchallenge.ui.article

import com.eticzon.ffxchallenge.data.model.Article
import com.eticzon.ffxchallenge.ui.base.BaseStatefulPresenter
import com.eticzon.ffxchallenge.ui.base.BaseView

/**
 * Contract for our [ArticleListActivity] related views, list item, and presenter interfaces.
 */
interface ArticleListContract {

    interface View : BaseView {
        fun setScreenTitle(title: String)
        fun showArticles(articles : List<Article>)
        fun showError(viewModel: ArticleListViewModel)
        fun showViewModel(viewModel: ArticleListViewModel)
    }

    interface Item {
        fun bind(article: Article, imageUrl: String)
    }

    interface Presenter : BaseStatefulPresenter<View, ArticleListViewModel> {
        fun getViewModel(): ArticleListViewModel
        fun presentListItem(item: Item, article: Article)
        fun refreshList()
        fun onItemClick(article: Article)
    }
}