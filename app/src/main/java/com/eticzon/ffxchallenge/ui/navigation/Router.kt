package com.eticzon.ffxchallenge.ui.navigation

import com.eticzon.ffxchallenge.ui.base.BaseView

interface Router {
    fun route(from: BaseView, to: Route)
}