package com.eticzon.ffxchallenge.ui.common

import android.view.View
import android.widget.ImageView

interface ImageLoader {
    fun load(view: View, url: String, into: ImageView)
}
