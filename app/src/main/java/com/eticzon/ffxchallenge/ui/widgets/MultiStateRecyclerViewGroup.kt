package com.eticzon.ffxchallenge.ui.widgets

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.eticzon.ffxchallenge.R

/**
 * A compound [View] we use to augment [RecyclerView] to display empty, loading, error, and success
 * states.
 *
 * It has methods that map to different states which we can also use to extend the behavior (e.g. to
 * override state transitions). At the moment it just hides/shows the [View] associated with the
 * request state.
 */
class MultiStateRecyclerViewGroup @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private val viewHolder: ArrayList<View> by lazy {
        arrayListOf<View>()
    }
    private val loadingView: LinearLayout by lazy {
        findViewById<LinearLayout>(R.id.loadingStateLayout)
    }
    private val emptyView: LinearLayout by lazy {
        findViewById<LinearLayout>(R.id.emptyStateLayout)
    }
    private val retryView: LinearLayout by lazy {
        findViewById<LinearLayout>(R.id.retryStateLayout)
    }
    private val retryButton : Button by lazy {
        findViewById<Button>(R.id.retryStateButton)
    }

    val recyclerView: RecyclerView by lazy {
        findViewById<RecyclerView>(R.id.recyclerView)
    }

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.widget_recyclerviewgroup, this, true)

        viewHolder.addAll(arrayListOf(recyclerView, loadingView, emptyView, retryView))
        transitionTo(recyclerView)
    }

    fun <T> setRetryCallback(callback: () -> T) {
        retryButton.setOnClickListener { callback() }
    }

    fun loading() {
        transitionTo(loadingView)
    }

    fun empty() {
        transitionTo(emptyView)
    }

    fun retry() {
        transitionTo(retryView)
    }

    fun success() {
        postDelayed({ transitionTo(recyclerView) }, 1000)
    }

    private fun transitionTo(viewToShow: View?) {
        viewHolder.filter { view -> view != viewToShow }
                .forEach { view -> view.visibility = View.GONE }
        viewToShow?.visibility = View.VISIBLE
    }
}