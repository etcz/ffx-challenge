package com.eticzon.ffxchallenge.ui.navigation

data class Route(val name: RouteName, val url: String)
