package com.eticzon.ffxchallenge.ui.article

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.eticzon.ffxchallenge.R
import com.eticzon.ffxchallenge.data.model.Article
import com.eticzon.ffxchallenge.ui.common.ImageLoader

class ArticleListAdapter(
        private val articles: List<Article>,
        private val presenter: ArticleListContract.Presenter,
        private val imageLoader: ImageLoader
) : RecyclerView.Adapter<ArticleViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_article_list, parent, false)
        return ArticleViewHolder(view, presenter::onItemClick, imageLoader)
    }

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        presenter.presentListItem(holder, articles[position])
    }

    override fun getItemCount(): Int {
        return articles.size
    }
}