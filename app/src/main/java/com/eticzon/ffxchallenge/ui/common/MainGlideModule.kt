package com.eticzon.ffxchallenge.ui.common

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
class MainGlideModule : AppGlideModule()