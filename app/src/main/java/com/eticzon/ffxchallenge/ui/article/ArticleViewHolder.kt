package com.eticzon.ffxchallenge.ui.article

import android.support.v7.widget.RecyclerView
import android.view.View
import com.eticzon.ffxchallenge.data.model.Article
import com.eticzon.ffxchallenge.extensions.relativeElapsedTime
import com.eticzon.ffxchallenge.ui.common.ImageLoader
import kotlinx.android.synthetic.main.item_article_list.view.*

class ArticleViewHolder(
        val view: View,
        private val onClick: (article: Article) -> Unit,
        private val imageLoader: ImageLoader
) : RecyclerView.ViewHolder(view), ArticleListContract.Item, View.OnClickListener {

    private lateinit var currentArticle: Article

    init {
        itemView.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        onClick(currentArticle)
    }

    override fun bind (article: Article, imageUrl: String) {
        currentArticle = article.copy()

        with (article) {
            itemView.headline.text = headline
            itemView.theAbstract.text = theAbstract
            itemView.byLine.text = formatByLine()
            itemView.lastModified.text = lastModified.relativeElapsedTime()
        }

        if (imageUrl.isNotBlank()) {
            imageLoader.load(view = itemView, url = imageUrl, into = itemView.articleImage)
        }
    }
}