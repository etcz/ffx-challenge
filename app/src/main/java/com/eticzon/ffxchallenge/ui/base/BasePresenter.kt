package com.eticzon.ffxchallenge.ui.base

interface BasePresenter<out V: BaseView> {

    val view: V?

    fun attach() {}

    fun detach() {}
}
