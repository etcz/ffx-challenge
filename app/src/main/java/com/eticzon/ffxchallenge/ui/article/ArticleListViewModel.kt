package com.eticzon.ffxchallenge.ui.article

import com.eticzon.ffxchallenge.data.model.Article
import com.eticzon.ffxchallenge.data.repository.RequestError
import com.eticzon.ffxchallenge.data.repository.RequestState
import com.eticzon.ffxchallenge.ui.base.BaseViewModel
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

/**
 * A custom view model class for the [ArticleListActivity] that is used to persist the UI state
 * (used during configuration changes).
 */
@PaperParcel
data class ArticleListViewModel(
        var title: String,
        var articles: List<Article>?,
        var requestState: RequestState,
        var errorMessage: String,
        var errorType: RequestError
): BaseViewModel, PaperParcelable {

    companion object {
        @JvmField val CREATOR = PaperParcelArticleListViewModel.CREATOR

        fun newInstance(): ArticleListViewModel {
            return ArticleListViewModel(
                    title = "",
                    articles = listOf(),
                    requestState = RequestState.IDLE,
                    errorMessage = "",
                    errorType = RequestError.NONE)
        }
    }
}