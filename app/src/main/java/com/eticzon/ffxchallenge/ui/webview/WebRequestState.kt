package com.eticzon.ffxchallenge.ui.webview

import com.eticzon.ffxchallenge.data.repository.RequestState

data class WebRequestState(
        val state: RequestState = RequestState.IDLE,
        val progress: Int = 0,
        val t: Throwable? = null
)
