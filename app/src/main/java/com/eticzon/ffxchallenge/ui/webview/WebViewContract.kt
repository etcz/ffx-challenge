package com.eticzon.ffxchallenge.ui.webview

import com.eticzon.ffxchallenge.ui.base.BasePresenter
import com.eticzon.ffxchallenge.ui.base.BaseView

interface WebViewContract {

    interface View : BaseView {
        fun loadContent()
        fun setLoadProgress(percent: Int)
    }

    interface Presenter : BasePresenter<View>
}