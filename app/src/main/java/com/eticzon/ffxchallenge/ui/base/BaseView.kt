package com.eticzon.ffxchallenge.ui.base

import com.eticzon.ffxchallenge.data.repository.RequestError

interface BaseView {
    fun showLoading()
    fun showProgress()
    fun hideProgress()
    fun showError(text: String, type: RequestError)
}
