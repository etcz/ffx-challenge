package com.eticzon.ffxchallenge.ui.common

import android.view.View
import android.widget.ImageView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.eticzon.ffxchallenge.R

class GlideImageLoader : ImageLoader {

    override fun load(view: View, url: String, into: ImageView) {
        GlideApp
                .with(view)
                .load(url)
                .centerCrop()
                .transition(withCrossFade(400))
                .error(R.drawable.ic_error_placeholder)
                .into(into)
    }
}