package com.eticzon.ffxchallenge.ui.widgets

import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.eticzon.ffxchallenge.R
import com.eticzon.ffxchallenge.data.repository.RequestError
import kotlinx.android.synthetic.main.fragment_transition.*

/**
 * A [Fragment] used to transition in-between the article list view to the actual article itself.
 *
 * It is also used to display any errors when fetching the article URL.
 */
class TransitionFragment : Fragment() {

    companion object {
        const val kTransitionType = "transitionType"
        const val kErrorMessage = "errorMessage"
        const val kErrorType = "errorType"

        fun loadingInstance(): TransitionFragment {
            val f = TransitionFragment()
            f.arguments = Bundle().apply {
                putInt(kTransitionType, TransitionType.LOADING.ordinal)
            }
            return f
        }

        fun errorInstance(text: String, type: RequestError): TransitionFragment {
            val f = TransitionFragment()
            f.arguments = Bundle().apply {
                putInt(kTransitionType, TransitionType.ERROR.ordinal)
                putInt(kErrorType, type.ordinal)
                putString(kErrorMessage, text)
            }
            return f
        }
    }

    enum class TransitionType {
        LOADING,
        ERROR
    }

    var loadProgress: Int = 0
        set (percent) {
            progressBar?.progress = percent
            field = percent
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_transition, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with (progressBar) {
            progressDrawable = progressDrawable.mutate().apply {
                setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN)
            }
        }

        when (arguments?.getInt(kTransitionType)) {
            TransitionType.LOADING.ordinal -> {
                progressBar.visibility = View.VISIBLE
                loadingImageView.visibility = View.VISIBLE
                errorImageView.visibility = View.GONE
                errorMessage.visibility = View.GONE
                errorMessage.text = ""
            }
            TransitionType.ERROR.ordinal -> {
                progressBar.visibility = View.GONE
                loadingImageView.visibility = View.GONE
                errorImageView.visibility = View.VISIBLE
                errorMessage.visibility = View.VISIBLE
                errorMessage.text = arguments?.getString(kErrorMessage) ?: ""
            }
        }

        when (arguments?.getInt(kErrorType)) {
            RequestError.TIMEOUT.ordinal -> {
                errorImageView.setImageResource(R.drawable.ic_error_timeout)
            }
            else -> {
                errorImageView.setImageResource(R.drawable.ic_error_404)
            }
        }
    }
}