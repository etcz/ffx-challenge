package com.eticzon.ffxchallenge.ui.webview

import com.eticzon.ffxchallenge.data.repository.RequestError
import com.eticzon.ffxchallenge.data.repository.RequestState
import com.eticzon.ffxchallenge.extensions.applySchedulers
import com.eticzon.ffxchallenge.scheduler.SchedulerFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException
import javax.inject.Inject

class WebViewPresenter @Inject constructor(
        override val view: WebViewContract.View,
        private val client: ObservableWebClient,
        private val schedulerFactory: SchedulerFactory
) : WebViewContract.Presenter {

    private val disposables = CompositeDisposable()

    override fun attach() {
        view.loadContent()
        disposables.add(setupSubscription())
    }

    override fun detach() {
        disposables.clear()
    }

    private fun setupSubscription(): Disposable {
        return client.getObservable()
                .timeout(10, TimeUnit.SECONDS)
                .compose(applySchedulers(schedulerFactory.io(), schedulerFactory.ui()))
                .takeUntil { item -> item.state in arrayOf(RequestState.COMPLETE, RequestState.ERROR) }
                .subscribeWith(getSubscriber())
    }

    private fun getSubscriber(): DisposableObserver<WebRequestState> {
        return object: DisposableObserver<WebRequestState>() {
            override fun onStart() {
                view.showProgress()
            }

            override fun onComplete() {
            }

            override fun onNext(item: WebRequestState) {
                when (item.state) {
                    RequestState.IDLE -> view.showProgress()
                    RequestState.LOADING -> view.setLoadProgress(item.progress)
                    RequestState.COMPLETE -> view.hideProgress()
                    RequestState.ERROR -> view.showError(item.t?.message ?: "", RequestError.SERVER)
                }
            }

            override fun onError(e: Throwable) {
                if (e is TimeoutException) {
                    view.showError("Request timed out", RequestError.TIMEOUT)
                } else {
                    view.showError(e.message ?: "", RequestError.OTHER)
                }
            }
        }
    }
}