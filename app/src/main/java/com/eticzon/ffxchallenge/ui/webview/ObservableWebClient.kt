package com.eticzon.ffxchallenge.ui.webview

import android.webkit.WebChromeClient
import android.webkit.WebViewClient
import io.reactivex.subjects.BehaviorSubject

interface ObservableWebClient {
    val webViewClient: WebViewClient
    val webChromeClient: WebChromeClient
    fun getObservable(): BehaviorSubject<WebRequestState>
}