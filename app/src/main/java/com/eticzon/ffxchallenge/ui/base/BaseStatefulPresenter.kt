package com.eticzon.ffxchallenge.ui.base

interface BaseStatefulPresenter<out V: BaseView, in S: BaseViewModel> : BasePresenter<V> {

    fun attach(saved: S?)
}