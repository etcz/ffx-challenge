package com.eticzon.ffxchallenge.ui.article

import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import com.eticzon.ffxchallenge.R
import com.eticzon.ffxchallenge.data.model.Article
import com.eticzon.ffxchallenge.data.repository.RequestError
import com.eticzon.ffxchallenge.data.repository.RequestState
import com.eticzon.ffxchallenge.di.AppComponent
import com.eticzon.ffxchallenge.di.ArticleListModule
import com.eticzon.ffxchallenge.ui.base.BaseActivity
import com.eticzon.ffxchallenge.ui.common.ImageLoader
import kotlinx.android.synthetic.main.activity_article_list.*
import javax.inject.Inject

class ArticleListActivity : BaseActivity(), ArticleListContract.View {

    companion object {
        const val KEY_VIEW_MODEL = "key.view_model"
    }

    @Inject lateinit var presenter: ArticleListPresenter
    @Inject lateinit var imageLoader: ImageLoader

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_list)
        setupViews()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        val saved = savedInstanceState?.let { readFromBundle(savedInstanceState) }
        presenter.attach(saved)
    }

    override fun onStop() {
        super.onStop()
        presenter.detach()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        writeToBundle(outState, presenter.getViewModel())
    }

    override fun injectDependencies(appComponent: AppComponent) {
        appComponent.plus(ArticleListModule(this))
                .injectTo(this)
    }

    override fun showLoading() {
        recyclerViewGroup.loading()
    }

    override fun showProgress() {
        refreshLayout.isRefreshing = true
    }

    override fun hideProgress() {
        refreshLayout.isRefreshing = false
    }

    override fun showError(text: String, type: RequestError) {
        refreshLayout.isRefreshing = false
        recyclerViewGroup.retry()
    }

    override fun showError(viewModel: ArticleListViewModel) {
        refreshLayout.isRefreshing = false
        recyclerViewGroup.retry()
    }

    override fun setScreenTitle(title: String) {
        (customToolbar as Toolbar).title = title
    }

    override fun showArticles(articles: List<Article>) {
        refreshLayout.isRefreshing = false
        if (articles.isNotEmpty()) {
            recyclerViewGroup.recyclerView.adapter = ArticleListAdapter(articles, presenter, imageLoader)
            recyclerViewGroup.success()
        } else {
            recyclerViewGroup.empty()
        }
    }

    override fun showViewModel(model: ArticleListViewModel) {
        val (title, articles, requestState, errorMessage, errorType) = model
        when (requestState) {
            RequestState.IDLE, RequestState.LOADING -> {
                showProgress()
            }
            RequestState.COMPLETE -> {
                hideProgress()
                setScreenTitle(title)
                showArticles(articles ?: listOf())
            }
            RequestState.ERROR -> {
                setScreenTitle(title)
                showError(errorMessage, errorType)
            }
        }
    }

    private fun setupViews() {
        setSupportActionBar(customToolbar as Toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        refreshLayout.setOnRefreshListener { presenter.refreshList() }
        refreshLayout.setColorSchemeResources(R.color.app_primary)

        recyclerViewGroup.recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerViewGroup.setRetryCallback {
            recyclerViewGroup.loading()
            refreshLayout.isRefreshing = true
            // Delay the call to fetch the API results to avoid "flashes" in the UI.
            refreshWithDelay(1000)
        }
    }

    private fun refreshWithDelay(delay: Long) {
        Handler().postDelayed({ presenter.refreshList() }, delay)
    }

    private fun readFromBundle(bundle: Bundle): ArticleListViewModel {
        return bundle.getParcelable(KEY_VIEW_MODEL)
    }

    private fun writeToBundle(outState: Bundle?, model: ArticleListViewModel) {
        outState?.putParcelable(KEY_VIEW_MODEL, model)
    }
}
