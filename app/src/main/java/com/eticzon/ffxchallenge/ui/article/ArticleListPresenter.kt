package com.eticzon.ffxchallenge.ui.article

import com.eticzon.ffxchallenge.data.model.Article
import com.eticzon.ffxchallenge.data.model.AssetList
import com.eticzon.ffxchallenge.data.repository.AssetRepository
import com.eticzon.ffxchallenge.data.repository.RequestError
import com.eticzon.ffxchallenge.data.repository.RequestState
import com.eticzon.ffxchallenge.ui.navigation.Route
import com.eticzon.ffxchallenge.ui.navigation.RouteName
import com.eticzon.ffxchallenge.ui.navigation.Router
import io.reactivex.disposables.CompositeDisposable
import retrofit2.Response
import timber.log.Timber
import javax.inject.Inject

class ArticleListPresenter @Inject constructor(
        override val view: ArticleListContract.View,
        private val router: Router,
        private val repository: AssetRepository
): ArticleListContract.Presenter {

    private val disposables = CompositeDisposable()
    private var viewModel = ArticleListViewModel.newInstance()

    override fun attach(saved: ArticleListViewModel?) {
        if (saved == null || saved.requestState in arrayOf(RequestState.IDLE, RequestState.LOADING)) {
            makeRequest(refresh = false)
            view.showLoading()
        } else {
            viewModel = saved.copy()
            view.showViewModel(saved)
        }
    }

    override fun detach() {
        disposables.clear()
    }

    override fun getViewModel(): ArticleListViewModel = viewModel.copy()

    override fun presentListItem(item: ArticleListContract.Item, article: Article) {
        item.bind(article, findSmallestImage(article))
    }

    override fun refreshList() {
        makeRequest(refresh = true)
    }

    override fun onItemClick(article: Article) {
        router.route(view, Route(RouteName.WEB_VIEW, article.url))
    }

    private fun makeRequest(refresh: Boolean) {
        val request = repository.getAssetList(refresh)
                .subscribe(this::handleResponse, this::handleError)

        val state = repository.getRequestState()
                .subscribe(this::handleRequestStateChange)

        disposables.add(request)
        disposables.add(state)
    }

    private fun handleResponse(response: Response<AssetList>) {
        Timber.d("Response code: %s", response.code())

        when (response.code()) {
            in 400..499 -> {
                viewModel.requestState = RequestState.ERROR
                viewModel.errorMessage = "Client error"
                viewModel.errorType = RequestError.CLIENT
                view.showError(viewModel)
                return
            }
            in 500..599 -> {
                viewModel.requestState = RequestState.ERROR
                viewModel.errorMessage = response.errorBody().toString()
                viewModel.errorType = RequestError.SERVER
                view.showError(viewModel)
                return
            }
        }

        response.body()?.let {
            val articles = it.assets
                    .filter { asset -> asset is Article }
                    .map { asset -> asset as Article }
                    .sortedByDescending { it.lastModified.millis }

            viewModel.requestState = RequestState.COMPLETE
            viewModel.articles = articles
            viewModel.title = notMadeInCuppertino(it.displayName)
            view.setScreenTitle(viewModel.title)
            view.showArticles(articles)
        }
    }

    private fun handleError(e: Throwable) {
        viewModel.requestState = RequestState.ERROR
        viewModel.errorMessage = e.message ?: ""
        viewModel.errorType = RequestError.OTHER
        view.showError(viewModel)
    }

    private fun handleRequestStateChange(newState: RequestState) {
        when (newState) {
            RequestState.IDLE, RequestState.LOADING -> view.showProgress()
            RequestState.COMPLETE -> view.hideProgress()
            else -> Timber.d("Unhandled state: %s", newState)
        }
    }

    private fun findSmallestImage(article: Article): String {
        val image = article.relatedImages.minBy { it.width * it.height }
        return image?.url ?: ""
    }

    private fun notMadeInCuppertino(text: String): String
        = text.replace(Regex("(iPhone|iPad|iOS|Apple)\\s*", RegexOption.IGNORE_CASE), "")
}