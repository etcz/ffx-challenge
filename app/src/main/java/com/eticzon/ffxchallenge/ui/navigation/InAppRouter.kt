package com.eticzon.ffxchallenge.ui.navigation

import android.content.Context
import android.content.Intent
import android.support.v4.util.SimpleArrayMap
import com.eticzon.ffxchallenge.ui.base.BaseActivity
import com.eticzon.ffxchallenge.ui.base.BaseView
import com.eticzon.ffxchallenge.ui.webview.WebViewActivity
import timber.log.Timber

class InAppRouter : Router {

    private val mappings by lazy {
        SimpleArrayMap<RouteName, Class<out BaseActivity>>()
    }

    init {
        mappings.put(RouteName.WEB_VIEW, WebViewActivity::class.java)
    }

    override fun route(from: BaseView, to: Route) {
        Timber.d("Routing to: %s", to.name)

        val destinationActivity = mappings.get(to.name)

        if (destinationActivity != null && from is Context) {
            val intent = Intent(from, destinationActivity).apply {
                putExtra(WebViewActivity.PARAM_EXTERNAL_URL, to.url)
                addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            from.startActivity(intent)
        }
    }
}