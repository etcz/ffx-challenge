package com.eticzon.ffxchallenge.di

import android.content.Context
import com.eticzon.ffxchallenge.BuildConfig
import com.eticzon.ffxchallenge.data.model.*
import com.eticzon.ffxchallenge.data.repository.AssetRepository
import com.eticzon.ffxchallenge.data.repository.AssetRepositoryImpl
import com.eticzon.ffxchallenge.data.repository.AssetService
import com.eticzon.ffxchallenge.gson.DateTimeTypeAdapter
import com.eticzon.ffxchallenge.gson.RuntimeTypeAdapterFactory
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.joda.time.DateTime
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * The [dagger.Module] where all our networking/data layer related dependencies should be
 * constructed.
 */
@Module
class RepositoryModule(private val context: Context) {

    @Provides
    fun provideContext(): Context = context

    @Provides @Singleton
    fun provideOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        }

        val builder = OkHttpClient().newBuilder()
                .followRedirects(true)
                .followSslRedirects(true)
        with (builder.networkInterceptors()) {
            add(logging)
            add(StethoInterceptor())
        }

        return builder.build()
    }

    @Provides
    fun provideBaseUrl(): String = BuildConfig.API_BASE_URL

    @Provides
    fun provideRuntimeTypeAdapterFactory(): RuntimeTypeAdapterFactory<Asset> {
        return RuntimeTypeAdapterFactory
                .of(Asset::class.java, "assetType")
                .registerSubtype(AssetList::class.java, AssetList.TYPE_STRING)
                .registerSubtype(Article::class.java, Article.TYPE_STRING)
                .registerSubtype(Html::class.java, Html.TYPE_STRING)
                .registerSubtype(Image::class.java, Image.TYPE_STRING)
                .registerSubtype(Video::class.java, Video.TYPE_STRING)
                .registerSubtype(LiveArticle::class.java, LiveArticle.TYPE_STRING)
    }

    @Provides
    fun provideGsonObj(assetTypeAdapterFactory: RuntimeTypeAdapterFactory<Asset>): Gson {
        return GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
                .registerTypeAdapter(object : TypeToken<DateTime>() {}.type, DateTimeTypeAdapter())
                .registerTypeAdapterFactory(assetTypeAdapterFactory)
                .create()
    }

    @Provides @Singleton
    fun provideRestAdapter(client: OkHttpClient, baseUrl: String, gson: Gson): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides @Singleton
    fun provideAssetService(retrofit: Retrofit): AssetService
            = retrofit.create(AssetService::class.java)

    @Provides @Singleton
    fun provideAssetRepository(assetRepositoryImpl: AssetRepositoryImpl): AssetRepository
            = assetRepositoryImpl
}