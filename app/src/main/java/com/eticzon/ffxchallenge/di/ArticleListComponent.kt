package com.eticzon.ffxchallenge.di

import com.eticzon.ffxchallenge.ui.article.ArticleListActivity
import dagger.Subcomponent

/**
 * [dagger.Component] for our [ArticleListActivity] class.
 */
@ActivityScope
@Subcomponent(modules = arrayOf(
        ArticleListModule::class
))
interface ArticleListComponent {

    fun injectTo(activity: ArticleListActivity)
}