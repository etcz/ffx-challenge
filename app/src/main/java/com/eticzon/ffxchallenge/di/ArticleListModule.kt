package com.eticzon.ffxchallenge.di

import com.eticzon.ffxchallenge.ui.article.ArticleListActivity
import com.eticzon.ffxchallenge.ui.article.ArticleListContract
import com.eticzon.ffxchallenge.ui.common.GlideImageLoader
import com.eticzon.ffxchallenge.ui.common.ImageLoader
import dagger.Module
import dagger.Provides

/**
 * [dagger.Module] for providing dependencies required by the [ArticleListActivity] class.
 */
@Module
class ArticleListModule(val view: ArticleListContract.View) {

    @Provides @ActivityScope
    fun provideView(): ArticleListContract.View = view

    @Provides @ActivityScope
    fun provideImageLoader(): ImageLoader = GlideImageLoader()
}