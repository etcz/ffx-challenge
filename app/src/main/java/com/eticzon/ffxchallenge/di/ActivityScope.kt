package com.eticzon.ffxchallenge.di

import javax.inject.Scope

/**
 *  Custom dagger scope we use for instances which live as long as the activity (e.g. presenters).
 */
@Scope
annotation class ActivityScope