package com.eticzon.ffxchallenge.di

import com.eticzon.ffxchallenge.ui.webview.ObservableWebClient
import com.eticzon.ffxchallenge.ui.webview.ProgressTrackerWebClient
import com.eticzon.ffxchallenge.ui.webview.WebViewActivity
import com.eticzon.ffxchallenge.ui.webview.WebViewContract
import dagger.Module
import dagger.Provides

/**
 * [dagger.Component] for our [WebViewActivity] class.
 */
@Module
class WebViewModule(val view: WebViewContract.View) {

    @Provides @ActivityScope
    fun provideView(): WebViewContract.View = view

    @Provides @ActivityScope
    fun provideObservableWebClient(): ObservableWebClient = ProgressTrackerWebClient()
}