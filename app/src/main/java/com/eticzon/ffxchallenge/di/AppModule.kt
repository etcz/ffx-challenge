package com.eticzon.ffxchallenge.di

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import com.eticzon.ffxchallenge.scheduler.AppSchedulerFactory
import com.eticzon.ffxchallenge.scheduler.SchedulerFactory
import com.eticzon.ffxchallenge.ui.navigation.InAppRouter
import com.eticzon.ffxchallenge.ui.navigation.Router
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * The application [dagger.Module] where all Android-specific (and app-wide) dependencies should be
 * placed.
 */
@Module
class AppModule(private val appContext: Context) {

    @Provides @Singleton @AppContext
    fun provideApplicationContext(): Context = appContext

    @Provides @Singleton
    fun provideSharedPreferences(@AppContext context: Context): SharedPreferences = context.getSharedPreferences(context.packageName, MODE_PRIVATE)

    @Provides @Singleton
    fun provideRouter(): Router = InAppRouter()

    @Provides @Singleton
    fun provideSchedulerFactory(): SchedulerFactory = AppSchedulerFactory()
}