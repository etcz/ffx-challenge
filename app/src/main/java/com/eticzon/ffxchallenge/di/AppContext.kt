package com.eticzon.ffxchallenge.di

import javax.inject.Qualifier

/**
 * Custom dagger qualifier used to mark a context as being the application one.
 */
@Qualifier
annotation class AppContext