package com.eticzon.ffxchallenge.di

import com.eticzon.ffxchallenge.App
import com.eticzon.ffxchallenge.data.repository.AssetRepositoryImpl
import dagger.Component
import javax.inject.Singleton

/**
 * Main [dagger.Component] of the app where the non-Activity injections points are defined.
 *
 * Each Activity/Fragment in the app meant extend the object graph (by providing its own sub-graph,
 * in its own [dagger.Module]).
 */
@Singleton
@Component(modules = arrayOf(
        AppModule::class,
        RepositoryModule::class
))
interface AppComponent {

    fun plus(module: ArticleListModule): ArticleListComponent

    fun plus(module: WebViewModule): WebViewComponent

    fun inject(assetRepositoryImpl: AssetRepositoryImpl)

    fun inject(app: App)
}