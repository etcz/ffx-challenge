package com.eticzon.ffxchallenge.di

import com.eticzon.ffxchallenge.ui.webview.WebViewActivity
import dagger.Subcomponent

/**
 * [dagger.Component] for our [WebViewActivity] class.
 */
@ActivityScope
@Subcomponent(modules = arrayOf(
        WebViewModule::class
))
interface WebViewComponent {

    fun injectTo(activity: WebViewActivity)
}