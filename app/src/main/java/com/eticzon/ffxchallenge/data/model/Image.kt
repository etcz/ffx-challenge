package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Image(
        @SerializedName("url")
        val url: String,

        @SerializedName("width")
        val width: Int,

        @SerializedName("height")
        val height: Int
) : Asset, PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelImage.CREATOR
        val TYPE_STRING = "IMAGE"
    }
}
