package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Video(
        @SerializedName("id")
        val id: Int
) : Asset, PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelVideo.CREATOR
        val TYPE_STRING = "VIDEO"
    }
}