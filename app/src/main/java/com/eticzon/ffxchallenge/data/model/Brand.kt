package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Brand (
        @SerializedName("title")
        val title: String,

        @SerializedName("orderNum")
        val orderNum: Int
) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelBrand.CREATOR
    }
}
