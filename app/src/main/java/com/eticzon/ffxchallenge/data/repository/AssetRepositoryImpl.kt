package com.eticzon.ffxchallenge.data.repository

import com.eticzon.ffxchallenge.data.model.AssetList
import com.eticzon.ffxchallenge.extensions.applySchedulers
import com.eticzon.ffxchallenge.scheduler.SchedulerFactory
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 *  Main repository class.
 *
 *  We use a [BehaviorSubject] to decouple the loading states from the main observable tied to the
 *  request. Request states are denoted by the enum [RequestState].
 *
 *  @param service Service layer (our Retrofit interface).
 *  @param schedulerFactory Provider for the different [Scheduler], that can be used when
 *  subscribing to and observing on an [Observable].
 */
@Singleton
class AssetRepositoryImpl @Inject constructor(
        private val service: AssetService,
        private val schedulerFactory: SchedulerFactory
): AssetRepository {

    private var result: Observable<Response<AssetList>> = getRequestObservable()

    private val state: BehaviorSubject<RequestState> by lazy {
        BehaviorSubject.createDefault(RequestState.IDLE)
    }

    override fun getAssetList(refresh: Boolean): Observable<Response<AssetList>> {
        // Unless a (force) refresh is requested, we always return the cached Observable.
        if (refresh) {
            result = getRequestObservable()
        }
        return result
    }

    override fun getRequestState(): Observable<RequestState> = state

    private fun getRequestObservable(): Observable<Response<AssetList>> {
        return service.getAssetList()
                .compose(applySchedulers(schedulerFactory.io(), schedulerFactory.ui()))
                .doOnSubscribe { state.onNext(RequestState.IDLE) }
                .doOnNext { state.onNext(RequestState.LOADING) }
                .doOnError { state.onNext(RequestState.ERROR) }
                .doOnComplete { state.onNext(RequestState.COMPLETE) }
                .cache()
    }
}