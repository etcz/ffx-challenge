package com.eticzon.ffxchallenge.data.repository

import com.eticzon.ffxchallenge.data.model.AssetList
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET

interface AssetService {

    @GET("/1/coding_test/13ZZQX/full")
    fun getAssetList(): Observable<Response<AssetList>>
}