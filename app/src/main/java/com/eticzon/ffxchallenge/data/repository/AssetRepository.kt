package com.eticzon.ffxchallenge.data.repository

import com.eticzon.ffxchallenge.data.model.AssetList
import io.reactivex.Observable
import retrofit2.Response

/**
 *  Contract for implementing a data repository.
 *
 *  These include methods for fetching the data and getting the request states (i.e. loading,
 *  completed, error).
 */
interface AssetRepository {

    fun getAssetList(refresh: Boolean): Observable<Response<AssetList>>
    fun getRequestState(): Observable<RequestState>
}