package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Source(
		@SerializedName("tagId")
		val tagId: String
) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelSource.CREATOR
    }
}