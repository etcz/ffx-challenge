package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class ArticleOverride(
        @SerializedName("overrideAbstract")
        val overrideAbstract: String = "",

        @SerializedName("overrideHeadline")
        val overrideHeadline: String = "",

        @SerializedName("overrideIdentifier")
        val overrideIdentifier: String = ""
) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelArticleOverride.CREATOR
    }
}
