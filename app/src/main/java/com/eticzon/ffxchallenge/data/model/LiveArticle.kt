package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class LiveArticle(
        @SerializedName("id")
        val id: Int
) : Asset, PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelLiveArticle.CREATOR
        val TYPE_STRING = "LIVE_ARTICLE"
    }
}