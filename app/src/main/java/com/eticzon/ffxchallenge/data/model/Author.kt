package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Author(
        @SerializedName("name")
        val name: String,

        @SerializedName("title")
        val title: String,

        @SerializedName("email")
        val email: String,

        @SerializedName("relatedAssets")
        val relatedAssets: List<Asset>,

        @SerializedName("relatedImages")
        val relatedImages: List<Image>
) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelAuthor.CREATOR
    }
}
