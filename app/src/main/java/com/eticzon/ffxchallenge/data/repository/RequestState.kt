package com.eticzon.ffxchallenge.data.repository

enum class RequestState {
    IDLE,
    LOADING,
    COMPLETE,
    ERROR,
}