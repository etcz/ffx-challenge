package com.eticzon.ffxchallenge.data.model

import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
class NullAsset: Asset, PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelNullAsset.CREATOR
    }
}
