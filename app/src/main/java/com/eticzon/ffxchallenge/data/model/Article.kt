package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import org.joda.time.DateTime
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Article (
        @SerializedName("id")
        val id: Int,

        @SerializedName("categories")
        val categories: List<Category>,

        @SerializedName("authors")
        val authors: List<Author>,

        @SerializedName("brands")
        val brands: List<Brand>?,

        @SerializedName("url")
        val url: String,

        @SerializedName("lastModified")
        val lastModified: DateTime,

        @SerializedName("onTime")
        val onTime: Long,

        @SerializedName("sponsored")
        val sponsored: Boolean,

        @SerializedName("relatedImages")
        val relatedImages: List<Image>,

        @SerializedName("timeStamp")
        val timeStamp: Long,

        @SerializedName("identifier")
        val identifier: String,

        @SerializedName("byLine")
        val byLine: String,

        @SerializedName("acceptComments")
        val acceptComments: Boolean,

        @SerializedName("sources")
        val sources: List<Source>,

        @SerializedName("theAbstract")
        val theAbstract: String,

        @SerializedName("legalStatus")
        val legalStatus: String,

        @SerializedName("overrides")
        val override: ArticleOverride,

        @SerializedName("numberOfComments")
        val numberOfComments: Int,

        @SerializedName("companies")
        val companies: List<Company>,

        @SerializedName("tabletHeadline")
        val tabletHeadline: String,

        @SerializedName("indexHeadline")
        val indexHeadline: String,

        @SerializedName("headline")
        val headline: String
) : Asset, PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelArticle.CREATOR
        val TYPE_STRING = "ARTICLE"
    }

    fun formatByLine(): String {
        return byLine
                .split("\\s".toRegex())
                .map { if (it.equals("and", true)) "and" else it.toLowerCase().capitalize()}
                .joinToString(" ")
    }
}