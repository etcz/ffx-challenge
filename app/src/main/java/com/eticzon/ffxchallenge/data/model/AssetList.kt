package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class AssetList(
        @SerializedName("displayName")
        val displayName: String,

        @SerializedName("assets")
        val assets: List<Asset>
) : Asset, PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelAssetList.CREATOR
        val TYPE_STRING = "ASSET_LIST"
    }
}
