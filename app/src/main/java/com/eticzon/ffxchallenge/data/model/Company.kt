package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Company(
        @SerializedName("id")
        val id: Int,

        @SerializedName("companyCode")
        val companyCode: String,

        @SerializedName("companyName")
        val companyName: String,

        @SerializedName("companyNumber")
        val companyNumber: String,

        @SerializedName("abbreviatedName")
        val abbreviatedName: String,

        @SerializedName("exchange")
        val exchange: String
): PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelCompany.CREATOR
    }
}
