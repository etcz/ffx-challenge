package com.eticzon.ffxchallenge.data.repository

enum class RequestError {
    NONE,
    TIMEOUT,
    CLIENT,
    SERVER,
    OTHER,
}