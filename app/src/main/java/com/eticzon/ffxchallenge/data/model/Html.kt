package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Html(
        @SerializedName("embedCode")
        val embedCode: String
) : Asset, PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelHtml.CREATOR
        val TYPE_STRING = "HTML"
    }
}
