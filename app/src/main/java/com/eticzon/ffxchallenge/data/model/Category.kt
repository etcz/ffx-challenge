package com.eticzon.ffxchallenge.data.model

import com.google.gson.annotations.SerializedName
import paperparcel.PaperParcel
import paperparcel.PaperParcelable

@PaperParcel
data class Category(
        @SerializedName("name")
        val name: String,

        @SerializedName("orderNum")
        val orderNum: Int,

        @SerializedName("sectionPath")
        val sectionPath: String
) : PaperParcelable {
    companion object {
        @JvmField val CREATOR = PaperParcelCategory.CREATOR
    }
}
