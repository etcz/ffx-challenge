package com.eticzon.ffxchallenge.gson

import com.google.gson.*
import org.joda.time.DateTime
import java.lang.reflect.Type

/**
 * Custom [com.google.gson.JsonDeserializer] for the [org.joda.time.DateTime] class.
 *
 * It is (de)serialised as a [Long] (milliseconds from epoch).
 */
class DateTimeTypeAdapter : JsonSerializer<DateTime>, JsonDeserializer<DateTime> {

    override fun serialize(src: DateTime, srcType: Type, context: JsonSerializationContext): JsonElement {
        return JsonPrimitive(src.millis.toString())
    }

    @Throws(JsonParseException::class)
    override fun deserialize(json: JsonElement, type: Type, context: JsonDeserializationContext): DateTime? {
        if (json.asString.isNullOrEmpty()) {
            return null
        }

        return DateTime(json.asLong)
    }
}
