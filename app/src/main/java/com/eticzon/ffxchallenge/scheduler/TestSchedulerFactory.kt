package com.eticzon.ffxchallenge.scheduler

import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers

/**
 * [TestSchedulerFactory] uses [trampoline] so we can execute reactive code in tests on the current
 * thread and synchronously.
 */
class TestSchedulerFactory : SchedulerFactory {

    override fun ui(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun computation(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun trampoline(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun newThread(): Scheduler {
        return Schedulers.trampoline()
    }

    override fun io(): Scheduler {
        return Schedulers.trampoline()
    }
}