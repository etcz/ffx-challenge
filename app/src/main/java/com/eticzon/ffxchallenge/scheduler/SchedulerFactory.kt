package com.eticzon.ffxchallenge.scheduler

import io.reactivex.Scheduler

interface SchedulerFactory {

    fun ui(): Scheduler
    fun computation(): Scheduler
    fun trampoline(): Scheduler
    fun newThread(): Scheduler
    fun io(): Scheduler
}
