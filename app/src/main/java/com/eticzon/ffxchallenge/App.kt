package com.eticzon.ffxchallenge

import android.app.Application
import com.eticzon.ffxchallenge.di.*
import com.facebook.stetho.Stetho
import com.squareup.leakcanary.LeakCanary
import net.danlew.android.joda.JodaTimeAndroid
import timber.log.Timber

class App : Application() {

    companion object {
        lateinit var component: AppComponent
    }

    override fun onCreate() {
        super.onCreate()

        if (LeakCanary.isInAnalyzerProcess(this)) return
        LeakCanary.install(this)

        initDependencies()
        initDeveloperTools()

        JodaTimeAndroid.init(this)
    }

    private fun initDependencies() {
        component = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .repositoryModule(RepositoryModule(this))
                .build()
    }

    private fun initDeveloperTools() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
            Stetho.initializeWithDefaults(this)
        }
    }
}