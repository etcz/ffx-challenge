package com.eticzon.ffxchallenge.paperparcel

import android.os.Parcel
import com.eticzon.ffxchallenge.data.model.*
import paperparcel.TypeAdapter

/**
 * Custom PaperParcel [TypeAdapter] used so we can write a polymorphic [Asset] or List<[Asset]> type
 * to a [Parcel].
 *
 * PaperParcel's [TypeAdapter] are composable. Here we identify the type at runtime and invoke the
 * (type-specfic) generated [TypeAdapter] for that class.
 */
class AssetTypeAdapter(
        private val articleAdapter: TypeAdapter<Article>,
        private val imageAdapter: TypeAdapter<Image>,
        private val htmlAdapter: TypeAdapter<Html>,
        private val videoAdapter: TypeAdapter<Video>,
        private val liveArticleAdapter: TypeAdapter<LiveArticle>,
        private val nullAssetAdapter: TypeAdapter<NullAsset>
) : TypeAdapter<@JvmSuppressWildcards Asset> {

    override fun readFromParcel(source: Parcel): Asset {
        val assetType = source.readString()
        return when (assetType) {
            Article.TYPE_STRING -> articleAdapter.readFromParcel(source)
            Html.TYPE_STRING -> htmlAdapter.readFromParcel(source)
            Image.TYPE_STRING -> imageAdapter.readFromParcel(source)
            Video.TYPE_STRING -> videoAdapter.readFromParcel(source)
            LiveArticle.TYPE_STRING -> liveArticleAdapter.readFromParcel(source)
            else -> nullAssetAdapter.readFromParcel(source)
        }
    }

    override fun writeToParcel(asset: Asset, dest: Parcel, flags: Int) {
        when (asset) {
            is Article -> {
                dest.writeString(Article.TYPE_STRING)
                articleAdapter.writeToParcel(asset, dest, flags)
            }
            is Html -> {
                dest.writeString(Html.TYPE_STRING)
                htmlAdapter.writeToParcel(asset, dest, flags)
            }
            is Image -> {
                dest.writeString(Image.TYPE_STRING)
                imageAdapter.writeToParcel(asset, dest, flags)
            }
            is Video -> {
                dest.writeString(Video.TYPE_STRING)
                videoAdapter.writeToParcel(asset, dest, flags)
            }
            is LiveArticle -> {
                dest.writeString(LiveArticle.TYPE_STRING)
                liveArticleAdapter.writeToParcel(asset, dest, flags)
            }
            else -> nullAssetAdapter.writeToParcel(asset as NullAsset, dest, flags)
        }
    }
}