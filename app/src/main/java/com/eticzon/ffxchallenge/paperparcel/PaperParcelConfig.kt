package com.eticzon.ffxchallenge.paperparcel

import paperparcel.Adapter
import paperparcel.ProcessorConfig

/**
 * Configuration for PaperParcel's (https://grandstaish.github.io/paperparcel/) annotation processor
 * which we use to add a custom type adapter.
 */
@ProcessorConfig(adapters = arrayOf(Adapter(AssetTypeAdapter::class)))
interface PaperParcelConfig
