package com.eticzon.ffxchallenge.ui.article

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.eticzon.ffxchallenge.R
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ArticleListActivityTest {

    @Rule var activityRule: ActivityTestRule<ArticleListActivity> = ActivityTestRule(ArticleListActivity::class.java)

    @Test fun `was launched with list visible`() {
        onView(withId(R.id.recyclerViewGroup)).check(matches(isDisplayed()))
    }
}