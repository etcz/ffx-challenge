package com.eticzon.ffxchallenge.ui.article

import any
import capture
import com.eticzon.ffxchallenge.MockData
import com.eticzon.ffxchallenge.data.model.Article
import com.eticzon.ffxchallenge.data.model.AssetList
import com.eticzon.ffxchallenge.data.repository.AssetRepository
import com.eticzon.ffxchallenge.data.repository.RequestError
import com.eticzon.ffxchallenge.data.repository.RequestState
import com.eticzon.ffxchallenge.ui.navigation.InAppRouter
import com.eticzon.ffxchallenge.ui.navigation.Route
import com.eticzon.ffxchallenge.ui.navigation.RouteName
import eq
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*
import retrofit2.Response

class ArticleListPresenterTest {

    @Mock private lateinit var view: ArticleListContract.View

    @Mock private lateinit var repository: AssetRepository

    @Mock private lateinit var router: InAppRouter

    @InjectMocks private lateinit var presenter: ArticleListPresenter

    @Captor private lateinit var viewModelCaptor: ArgumentCaptor<ArticleListViewModel>

    @Captor private lateinit var responseCaptor: ArgumentCaptor<ArticleListViewModel>

    @Captor private lateinit var articlesCaptor: ArgumentCaptor<List<Article>>

    @Before fun setUp() {
        MockitoAnnotations.initMocks(this)

        val emptyAssetList = AssetList(displayName = "Breaking News", assets = listOf())
        val response = retrofit2.Response.success(emptyAssetList)

        `when`(repository.getAssetList(anyBoolean()))
                .thenReturn(Observable.defer { Observable.just(response) })
        `when`(repository.getRequestState())
                .thenReturn(Observable.just(RequestState.COMPLETE))
    }

    @Test fun `attaching with a saved state where request is done`() {
        presenter.attach(MockData.savedViewModelCompleted)

        verify(view).showViewModel(capture(viewModelCaptor))
        assertEquals(MockData.savedViewModelCompleted, viewModelCaptor.value)
    }

    @Test fun `attaching with a saved state where the request is in-flight`() {
        presenter.attach(MockData.savedViewModelInFlight)

        verify(repository).getAssetList(false)
        verify(repository).getRequestState()
        verify(view).showLoading()
    }

    @Test fun `attaching without any saved state`() {
        presenter.attach(null)

        verify(repository).getAssetList(false)
        verify(repository).getRequestState()
        verify(view).showLoading()
    }

    @Test fun `returns a copy of the view model`() {
        presenter.attach(MockData.savedViewModelCompleted)

        assertEquals(MockData.savedViewModelCompleted, presenter.getViewModel())
        assertFalse(MockData.savedViewModelCompleted === presenter.getViewModel())
    }

    @Test fun `uses the smallest image when binding data to a list item`() {
        val mockListItem = mock(ArticleListContract.Item::class.java)

        presenter.presentListItem(mockListItem, MockData.firstArticle)

        verify(mockListItem).bind(any<Article>(), eq(MockData.smallImage.url))
    }

    @Test fun `refreshing the list calls the repository methods correctly`() {
        presenter.refreshList()

        verify(repository).getAssetList(true)
        verify(repository).getRequestState()
    }

    @Test fun `clicking a list item routes to the correct screen`() {
        presenter.onItemClick(MockData.firstArticle)

        verify(router).route(view, Route(RouteName.WEB_VIEW, MockData.firstArticle.url))
    }

    @Test fun `doesn't call any view methods when detaching`() {
        presenter.detach()

        verifyZeroInteractions(view)
    }

    @Test fun `calls correct methods on 4XX errors`() {
        val body = ResponseBody.create(MediaType.parse("text/plain"), "Resource not found")
        val response = retrofit2.Response.error<AssetList>(404, body)

        `when`(repository.getAssetList(anyBoolean()))
                .thenReturn(Observable.defer { Observable.just(response) })
        `when`(repository.getRequestState())
                .thenReturn(Observable.just(RequestState.COMPLETE))

        presenter.refreshList()

        verify(view).hideProgress()
        verify(view).showError(capture(responseCaptor))
        assertEquals(responseCaptor.value.requestState, RequestState.ERROR)
        assertEquals(responseCaptor.value.errorType, RequestError.CLIENT)
    }

    @Test fun `calls correct methods on 5XX errors`() {
        val body = ResponseBody.create(MediaType.parse("text/plain"), "Internal server error")
        val response = retrofit2.Response.error<AssetList>(500, body)

        `when`(repository.getAssetList(anyBoolean()))
                .thenReturn(Observable.defer { Observable.just(response) })
        `when`(repository.getRequestState())
                .thenReturn(Observable.just(RequestState.COMPLETE))

        presenter.refreshList()

        verify(view).hideProgress()
        verify(view).showError(capture(responseCaptor))
        assertEquals(responseCaptor.value.requestState, RequestState.ERROR)
        assertEquals(responseCaptor.value.errorType, RequestError.SERVER)
    }

    @Test fun `handles errors from upstream Observable`() {
        `when`(repository.getAssetList(anyBoolean()))
                .thenReturn(Observable.error<Response<AssetList>>(Exception("Fake exception")))

        presenter.refreshList()

        verify(view).showError(capture(responseCaptor))
        assertEquals(responseCaptor.value.requestState, RequestState.ERROR)
        assertEquals(responseCaptor.value.errorType, RequestError.OTHER)
    }

    @Test fun `shows articles sorted by timestamp descending`() {
        val response = retrofit2.Response.success(MockData.assetListWithThreeArticles)
        val expectedOrder = listOf(MockData.thirdArticle, MockData.secondArticle, MockData.firstArticle)

        `when`(repository.getAssetList(anyBoolean()))
                .thenReturn(Observable.defer { Observable.just(response) })
        `when`(repository.getRequestState())
                .thenReturn(Observable.just(RequestState.COMPLETE))

        presenter.refreshList()

        verify(view).setScreenTitle(MockData.assetListWithThreeArticles.displayName)
        verify(view).showArticles(capture(articlesCaptor))
        assertThat(articlesCaptor.value, `is`(expectedOrder))
    }

    @Test fun `calls correct methods on request state changes`() {
        `when`(repository.getRequestState())
                .thenReturn(Observable.fromArray(RequestState.IDLE, RequestState.LOADING, RequestState.COMPLETE))

        presenter.refreshList()

        val orderVerifier = Mockito.inOrder(view)
        with (orderVerifier) {
            verify(view, times(2)).showProgress()
            verify(view).hideProgress()
            verifyNoMoreInteractions()
        }
    }
}