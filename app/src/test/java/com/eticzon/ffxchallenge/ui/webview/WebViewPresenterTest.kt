package com.eticzon.ffxchallenge.ui.webview

import com.eticzon.ffxchallenge.data.repository.RequestError
import com.eticzon.ffxchallenge.data.repository.RequestState
import com.eticzon.ffxchallenge.scheduler.TestSchedulerFactory
import eq
import io.reactivex.subjects.BehaviorSubject
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.concurrent.TimeoutException

class WebViewPresenterTest {

    @Mock private lateinit var view: WebViewContract.View

    @Mock private lateinit var client: ObservableWebClient

    private lateinit var presenter: WebViewPresenter

    private lateinit var subject: BehaviorSubject<WebRequestState>

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        presenter = WebViewPresenter(view, client, TestSchedulerFactory())
        subject = BehaviorSubject.create<WebRequestState>()

        `when`(client.getObservable()).thenReturn(subject)
    }

    @Test fun `calls correct methods on attach`() {
        presenter.attach()

        verify(view).loadContent()
    }

    @Test fun `doesn't have any view interactions on detach`() {
        presenter.detach()

        verifyZeroInteractions(view)
    }

    @Test fun `calls correct view methods on progress changes`() {
        val states = arrayOf(
                WebRequestState(state = RequestState.IDLE, progress = 0, t = null),
                WebRequestState(state = RequestState.LOADING, progress = 25, t = null),
                WebRequestState(state = RequestState.LOADING, progress = 50, t = null),
                WebRequestState(state = RequestState.LOADING, progress = 75, t = null),
                WebRequestState(state = RequestState.COMPLETE, progress = 100, t = null))

        presenter.attach()

        for (state in states) {
            subject.onNext(state)
        }

        val orderVerifier = Mockito.inOrder(view)
        with (orderVerifier) {
            verify(view).loadContent()
            verify(view, times(2)).showProgress()
            verify(view).setLoadProgress(states[1].progress)
            verify(view).setLoadProgress(states[2].progress)
            verify(view).setLoadProgress(states[3].progress)
            verify(view).hideProgress()
            verifyNoMoreInteractions()
        }
    }

    @Test fun `handles error from upstream Observable`() {
        subject.onError(Exception("Fake exception"))

        presenter.attach()

        verify(view).showError(anyString(), eq(RequestError.OTHER))
    }

    @Test fun `handles network timeouts`() {
        subject.onError(TimeoutException("Request timed out"))

        presenter.attach()

        verify(view).showError(anyString(), eq(RequestError.TIMEOUT))
    }

    @Test fun `calls the correct methods if an error has occurred midway`() {
        val states = arrayOf(
                WebRequestState(state = RequestState.IDLE, progress = 0, t = null),
                WebRequestState(state = RequestState.LOADING, progress = 25, t = null),
                WebRequestState(state = RequestState.LOADING, progress = 50, t = null),
                WebRequestState(state = RequestState.ERROR, progress = 50, t = Exception("Network error")))

        presenter.attach()

        for (state in states) {
            subject.onNext(state)
        }

        val orderVerifier = Mockito.inOrder(view)
        with (orderVerifier) {
            verify(view).loadContent()
            verify(view, times(2)).showProgress()
            verify(view).setLoadProgress(states[1].progress)
            verify(view).setLoadProgress(states[2].progress)
            verify(view).showError(anyString(), eq(RequestError.SERVER))
        }
    }
}