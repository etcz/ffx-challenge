FFX Coding Challenge
====================

A take on FFX's Android coding challenge. Essentially a simple news feed app that uses data from this endpoint: https://bruce-v2-mob.fairfaxmedia.com.au/1/coding_test/13ZZQX/full.

This app follows a slightly modified MVP architecture pattern. A view model object is used at the presenter layer to enable a simple approach for handling configuration changes.
The view layer responsibility is kept to a [minimum](https://martinfowler.com/eaaDev/PassiveScreen.html). The data layer is modeled using the [repository pattern](https://martinfowler.com/eaaCatalog/repository.html).

![App still](app_still.png) ![Demo](app_demo.gif)

Setup
-----

* [Android Studio 3.0+](https://developer.android.com/studio/index.html)
* Android SDK Build Tools 27.0.3

Dependencies
------------

* [Kotlin Android Extensions](https://kotlinlang.org/docs/tutorials/android-plugin.html) - view binding
* [Dagger 2](https://github.com/google/dagger) - dependency injection
* [RxJava 2](https://github.com/ReactiveX/RxJava) - async task composition and handling
* [RxAndroid](https://github.com/ReactiveX/RxAndroid) - only used for Android main thread [Scheduler](http://reactivex.io/documentation/scheduler.html)
* [Retrofit](https://github.com/square/retrofit) - REST adapter
* [Glide](https://github.com/bumptech/glide) - image loading
* [PaperParcel](https://grandstaish.github.io/paperparcel/) - automatic generation of Parcelable implementation
* [JodaTime](https://github.com/dlew/joda-time-android) - date/time handling
* [Calligraphy](https://github.com/chrisjenx/Calligraphy) - custom fonts
* Testing:
  * [JUnit4](https://github.com/junit-team/junit4)
  * [Mockito](https://github.com/mockito/mockito)
  * [Hamcrest](https://github.com/hamcrest/JavaHamcrest)
* Debugging tools:
  * [Timber](https://github.com/JakeWharton/timber)
  * [Stetho](https://github.com/facebook/stetho)
  * [LeakCanary](https://github.com/square/leakcanary)

Key points
----------

* Structure built with extensibility in mind.
* Article feed/list screen is a [RecyclerView](https://developer.android.com/reference/android/support/v7/widget/RecyclerView.html) wrapped in a [SwipeRefreshLayout](https://developer.android.com/reference/android/support/v4/widget/SwipeRefreshLayout.html).
* Rotation/orientation changes fully handled. Network requests persist across rotations.
* Error handling is **not** afterthought.
* API base URL defined in `build.gradle` so it can vary based on product flavors or build variants.
* Dependency versioning centralized in the root `build.gradle` file.

TODO
----

* More Espresso/instrumented tests.
* Offline support (using either [Room](https://developer.android.com/topic/libraries/architecture/room.html) or [Realm](https://github.com/realm/realm-java)).
* Tablet layouts.

Credits
-------
For icon attributions, please refer to the ATTRIBUTION file.
